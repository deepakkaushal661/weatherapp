import React, { useState, useEffect } from 'react'
import "./style.css";
import WeatherCard from './weatherCard';
const Temp = () => {
  const [searchvalue, setsearchvalue] = useState("pune");
  const [tempInfo, settempInfo] = useState({})
  const getWeatherInfo = async () => {
    try {
      let url = `https://api.openweathermap.org/data/2.5/weather?q=${searchvalue}&units=metric&appid=e0b6c297c47be4fcf27bd32dbeaa12b7`;

      const res = await fetch(url);
      const data = await res.json();
      console.log(data);

      const { temp, humidity, pressure } = data.main;
      console.log(temp);

      const { main: weathermood } = data.weather[0];

      const { name } = data;
      const { speed } = data.wind;
      const { country, sunset } = data.sys;

      const myNewWeatherInfo = {
        temp,
        humidity,
        pressure,
        weathermood,
        name,
        speed,
        country,
        sunset
      }

      settempInfo(myNewWeatherInfo)

    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getWeatherInfo();
  }, []);



  return (
    <>
      <div className='wrap'>
        <div className='search'>
          <input type="text" id='search' autoFocus className='searchTerm' placeholder='Search....' value={searchvalue} onChange={(e) => { setsearchvalue(e.target.value) }} />
          <button className='searchButton' onClick={getWeatherInfo}>Search</button>
        </div>
      </div>

      <WeatherCard  tempInfo = {tempInfo} />


    </>
  )
}

export default Temp;